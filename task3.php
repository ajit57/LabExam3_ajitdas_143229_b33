<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>A Chess Board (8X8) by using CSS (not table).</title>
    <style>
        *{margin:0;padding:0;}
        h1{text-align:center;}
        .main{width:98%;margin:0 auto;}
        .box{width:150px;height: 150px;background:green;text-align:center;display:inline-block;}


    </style>
</head>
<body>
    <h1>A Chess Board (8X8) by using CSS (not table).</h1>
    <div class="main">
        <?php
        for($i=1;$i<=8;$i++){
            for($j=1;$j<=8;$j++){

               if($i&1){

                   if($j&1){
                       echo '<div style="background:#000;" class="box"></div>';
                   }
                   else{
                       echo '<div style="background:yellow;" class="box"></div>';

                   }

               } else{

                   if($j&1){
                       echo '<div style="background:#fffa04;" class="box"></div>';
                   }
                   else{
                       echo '<div style="background:#000;" class="box"></div>';

                   }


               }
            }


        }
?>
    </div>
</body>
</html>