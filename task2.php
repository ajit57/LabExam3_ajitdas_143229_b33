<?php
 $temperatures = array(78, 60, 62, 68, 71, 68, 73, 85, 66, 64, 76, 63, 75, 76, 73, 68, 62, 73, 72, 65, 74,

     62, 62, 65, 64, 68, 73, 75, 79, 73);
$noTemperature = count($temperatures);
$sumTemperature = array_sum($temperatures);
$avgTemperature = $sumTemperature/$noTemperature;
$temperatureUnique = array_unique($temperatures);
$temperature_sort = sort($temperatureUnique);


echo 'Average Temperature is : ' . $avgTemperature . '<br>';

$lowest_number = implode(' ', array_slice($temperatureUnique, 0, 5 ));
$heighest_number = implode(' ', array_slice($temperatureUnique, -5, 5 ));

echo 'List of five lowest temperatures : '.$lowest_number;
echo 'List of five highest temperatures : '.$heighest_number;